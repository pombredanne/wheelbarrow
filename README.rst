Wheelbarrow
===========

Wheelbarrow is a command line tool for building Debian packages
containing python wheels and a requirements.txt file, when installed a
virtualenv is created with all requirements installed.

Because only the packaged wheel files are used, virtualenvs can be
deployed in production environments that are firewalled off from the
Internet, where the python package index cannot be used.

Packages can be built from multiple requirements.txt files, for example
if you have requirements.txt and production\_requirements.txt file.

Package metadata is in the wheelbarrow.json file, here you can specify
the location of your requirements.txt file(s), as well as package
information like the package name, version, package author and
description.

For this to work, you need at least pip version 1.5. Ubuntu 14.04 LTS
comes with pip 1.5, but older versions do not.

Installation using a virtualenv
-------------------------------

You need at least Ubuntu 14.04 for this, install python-virtualenv from
apt, or use virtualenvwrapper, then create a new virtualenv and make it
active::

    sudo apt-get install virtualenvwrapper
    mkvirtualenv wheelbarrow
    workon wheelbarrow

Note that mkvirtualenv generally makes the virtualenv active for you
anyway, so the extra "workon" command is probably not needed. You can
also use python 3, just add --python=/usr/bin/python3 to mkvirtualenv.

Clone the GIT repository::

    git clone https://bitbucket.org/robvdl/wheelbarrow.git

Installing wheelbarrow::

    cd wheelbarrow
    ./setup.py install

Creating a new project
----------------------

To create a new project, type::

    wheelbarrow init

You will be asked a few questions and a wheelbarrow.json file will be created.

Building a .deb package
-----------------------

To build a debian package, type::

    wheelbarrow build

This must be done in the folder where the wheelbarrow.json file is located.

Development and test environment
--------------------------------

The dev requirements are installed from a separate requirements.txt file::

    pip install -r requirements/development.txt

Alternatively, the following works too and does the same thing::

    pip install -e .[dev]

To run the tests in the current environment::

    nosetests

To run the tests against multiple python versions, you can use tox::

    tox

When using tox, test reports for Jenkins will be generated in the .tox folder.
