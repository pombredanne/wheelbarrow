import os
import shutil
import unittest

from mock import Mock, patch

from wheelbarrow.requirements import Requirements
from .utils import TEST_DIR, TEST_DATA


class RequirementsTests(unittest.TestCase):

    mock_config = {
        'dependency_links': [],
        'requirements': ['requirements.txt', 'prod_requirements.txt']
    }

    def setUp(self):
        shutil.rmtree(TEST_DIR, True)
        os.mkdir(TEST_DIR)

        # remember old directory
        self.old_dir = os.getcwd()
        os.chdir(TEST_DIR)

    def tearDown(self):
        # return to old directory
        os.chdir(self.old_dir)

    def _copy_test_files(self, files):
        """
        Copies files from the "tests/testdata" directory to the temporary
        test folder "/tmp/test-wheelbarrow".
        """
        for filename in files:
            src = os.path.join(TEST_DATA, filename)
            shutil.copy(src, '.')

    @patch('wheelbarrow.requirements.Requirements.parse_requirements', Mock())
    def test_constructor_calls_parse_requirements(self):
        """
        Only really ensures the constructor calls parse_requirements.
        """
        requirements = Requirements(self.mock_config)

        expected_args = self.mock_config['requirements']
        requirements.parse_requirements.assert_called_with(expected_args)

    @patch('wheelbarrow.requirements.Requirements.parse_requirements', Mock())
    def test_read_requirements_file(self):
        """
        Test for read_requirements_file method, which reads one
        requirements.txt file.
        """
        # copy test files
        self._copy_test_files(['requirements.txt'])

        requirements = Requirements(self.mock_config)
        lines = requirements.read_requirements_file('requirements.txt')

        # lines should have had \n characters removed
        self.assertListEqual(
            lines,  ['# this is a comment', 'Django==3.7', 'pip>=1.9'])

    def test_parse_requirements(self):
        """
        The parse_requirements method parses multiple requirements.txt
        files and populates the Requirements dict.

        Since the constructor calls parse_requirements anyway, we just call
        that instead.
        """
        # copy test files
        self._copy_test_files([
            'requirements.txt',
            'prod_requirements.txt',
            'bad_requirements.txt',
        ])

        # successfully loading two requirements.txt files
        requirements = Requirements(self.mock_config)
        self.assertDictEqual(
            requirements, {'psycopg2': '', 'django': '==3.7', 'pip': '>=1.9'})

        # this should raise an exception because -e was used
        with self.assertRaisesRegexp(ValueError, 'Unsupported line in'):
            Requirements({'requirements': ['bad_requirements.txt']})

    @patch('wheelbarrow.requirements.Requirements.parse_requirements', Mock())
    def test_save(self):
        """
        Test just the Requirements.save() method.
        """
        requirements = Requirements({'requirements': []})
        requirements['Django'] = '1.6.3'
        requirements['pip'] = '>=1.5'
        requirements.save('requirements.txt')

        # check if file was written successfully
        with open('requirements.txt', 'r') as f:
            # sort first, as save() doesn't guarantee order
            lines = sorted([l.strip() for l in f.readlines()])
            self.assertListEqual(lines, ['Django1.6.3', 'pip>=1.5'])
