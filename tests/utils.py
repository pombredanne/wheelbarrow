import os

# directory used during testing, warning this is deleted during tests
TEST_DIR = '/tmp/test-wheelbarrow'

# location to testdata folder
TEST_DATA = os.path.join(os.path.dirname(__file__), 'testdata')
