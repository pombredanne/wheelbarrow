import os
import shutil
import unittest

from mock import Mock, patch, call

from wheelbarrow.config import Config
from .utils import TEST_DIR, TEST_DATA


class ConfigTests(unittest.TestCase):

    config_file = 'wheelbarrow.json'

    def setUp(self):
        shutil.rmtree(TEST_DIR, True)
        os.mkdir(TEST_DIR)

        # remember old directory
        self.old_dir = os.getcwd()
        os.chdir(TEST_DIR)

    def tearDown(self):
        # return to old directory
        os.chdir(self.old_dir)

    @patch('wheelbarrow.config.Config.load_config', Mock())
    def test_constructor_calls_load_config(self):
        """
        The constructor should call load_config to load the wheelbarrow.json
        config file, if one exists in the current directory.
        """
        # try without a config file in directory, load_config not called
        Config()
        self.assertFalse(Config.load_config.called)

        # now create a dummy config file
        open(self.config_file, 'w').close()

        # creating a new Config object should now call load_config
        Config()
        self.assertTrue(Config.load_config.called)

    def test_load_config(self):
        """
        Test for the load_config() method.
        """
        # create a new config object first
        config = Config()

        # copy test config
        src = os.path.join(TEST_DATA, 'wheelbarrow.json')
        shutil.copy(src, '.')

        # now call load_config() and check a few things
        config.load_config()
        self.assertEqual(config['package']['author'], 'Joe Bloggs')
        self.assertEqual(config['package']['version'], '0.1')

    def test_save_config(self):
        """
        Test for the save_config method.
        """
        # create a new config object, but keep it simple,
        # we are not testing the json library itself..
        config = Config(foo='bar')

        # save
        config.save()

        # now read file and check string
        with open(self.config_file, 'r') as f:
            self.assertEqual(f.read(), '{\n    "foo": "bar"\n}\n')

    @patch('wheelbarrow.config.Config.save', Mock())
    @patch('wheelbarrow.config.Config.load_config', Mock())
    @patch('wheelbarrow.config.Config.input_text', Mock())
    def test_generate_new_config__overwrite_existing_file(self):
        """
        Test that the generate_new_config() method asks the user to
        overwrite the existing wheelbarrow.json config file if it exists.
        """
        # the question used to overwrite the config file
        expected_call = call(
            'Config file {0} already exists, overwrite (y/N)? '
            .format(self.config_file))

        # config file doesn't exist yet, we shouldn't get asked,
        # save() method should be called.
        config = Config()
        config.generate_new_config()
        self.assertNotIn(expected_call, config.input_text.mock_calls)
        self.assertTrue(config.save.called)

        # reset mocks and create dummy config file,
        # when asked to overwrite config file, answer 'n'
        # this time, save() should NOT have been called.
        config.input_text.reset_mock()
        config.input_text.return_value = 'n'
        config.save.reset_mock()
        open(self.config_file, 'w').close()
        config = Config()
        config.generate_new_config()
        self.assertIn(expected_call, config.input_text.mock_calls)
        self.assertFalse(config.save.called)

        # try again, this time answer 'y'
        # this time, save() should have been called.
        config.input_text.reset_mock()
        config.input_text.return_value = 'y'
        config.save.reset_mock()
        config = Config()
        config.generate_new_config()
        self.assertIn(expected_call, config.input_text.mock_calls)
        self.assertTrue(config.save.called)

    @patch('wheelbarrow.config.Config.save', Mock())
    @patch('wheelbarrow.config.Config.input_text')
    def test_generate_new_config__result(self, input_mock):
        """
        Test that just checks the result that the generate_new_config()
        function generates when answering all questions.
        """
        # answer the questions
        input_mock.side_effect = [
            'example-wheels',         # package name
            '0.1',                    # package version
            'Package description',    # package description
            'John',                   # package author
            'john@example.com'        # package author email
        ]
        config = Config()
        config.generate_new_config()

        # check if everything is what we expect
        expected_config = {
            'requirements': ['requirements.txt'],
            'package': {
                'email': 'john@example.com',
                'author': 'John',
                'version': '0.1',
                'name': 'example-wheels',
                'description': 'Package description'
            }
        }
        self.assertDictEqual(config, expected_config)

    def test_get_python_version(self):
        """
        Return the python version from the config object as a float.

        If the version is unsupported, an exception should be thrown.
        """
        config = Config()

        config['package'] = {'python': '2'}
        self.assertEqual(config.get_python_version(), 2.0)

        config['package'] = {'python': '3'}
        self.assertEqual(config.get_python_version(), 3.0)

        config['package'] = {'python': '2.7'}
        self.assertEqual(config.get_python_version(), 2.7)

        config['package'] = {'python': '3.9'}
        self.assertEqual(config.get_python_version(), 3.9)

        config['package'] = {'python': '1.9'}
        with self.assertRaisesRegexp(ValueError, 'Unsupported python version'):
            config.get_python_version()

        config['package'] = {'python': '4.0'}
        with self.assertRaisesRegexp(ValueError, 'Unsupported python version'):
            config.get_python_version()
