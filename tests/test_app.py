import os
import json
import shutil
import unittest

from mock import patch, call, Mock

from wheelbarrow.app import Application
from .utils import TEST_DIR, TEST_DATA


class ApplicationTests(unittest.TestCase):

    config_file = 'wheelbarrow.json'

    class MockConfig(dict):
        """
        Mock the Config dict so we can add a method to it, and since we can't
        directly add mocked methods (or any methods) to the dict type without
        subclassing, also I prefer not to use the real Config object in these
        tests if possible and use mocks instead.
        """
        def get_python_version(self):
            return 2.0

    def setUp(self):
        shutil.rmtree(TEST_DIR, True)
        os.mkdir(TEST_DIR)

        # remember old directory
        self.old_dir = os.getcwd()
        os.chdir(TEST_DIR)

        # load test config
        with open(os.path.join(TEST_DATA, self.config_file), 'r') as f:
            self.config = self.MockConfig(json.load(f))

    def tearDown(self):
        # return to old directory
        os.chdir(self.old_dir)

    @patch('wheelbarrow.app.Requirements')
    def test_constructor(self, requirements):
        """
        Tests that the constructor creates the Requirements object and
        correctly passes the list at config['requirements'] as an argument.
        """
        Application(self.config)
        requirements.assert_called_once_with(self.config)

    @patch('subprocess.check_call')
    @patch('wheelbarrow.app.Requirements', Mock())
    def test_create_virtualenv(self, check_call):
        """
        The create_virtualenv() method should create a virtualenv and
        run "pip install wheel".
        """
        python_bin = '/usr/bin/python'
        expected_calls = [
            call(['virtualenv', '-p', python_bin, '/tmp/wheelbarrow/venv']),
            call(['/tmp/wheelbarrow/venv/bin/pip', 'install', 'wheel'])
        ]

        app = Application(self.config)
        app.create_virtualenv()

        self.assertListEqual(check_call.mock_calls, expected_calls)

    @patch('subprocess.check_call')
    @patch('wheelbarrow.app.Requirements', Mock())
    def test_build_wheels(self, check_call):
        full_requirements_file = '/tmp/wheelbarrow/build/requirements.txt'
        pip_requirements_file = '/tmp/wheelbarrow/build/pip_requirements.txt'

        expected_command = [
            '/tmp/wheelbarrow/venv/bin/pip',
            'wheel',
            '-r',
            '/tmp/wheelbarrow/build/pip_requirements.txt',
            '-w',
            '/tmp/wheelbarrow/build/wheelhouse'
        ]

        app = Application(self.config)
        app.requirements.save = Mock()
        app.build_wheels()

        # two requirements.txt files are saved
        self.assertListEqual(
            app.requirements.save.mock_calls,
            [
                call(full_requirements_file, False),
                call(pip_requirements_file, True)
            ]
        )

        # command to build wheels was called
        check_call.assert_called_once_with(expected_command)

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_get_package_date(self):
        """
        Test for the get_package_date() function, which should return
        a string version of the date, suitable for inserting into Debian
        changelog files.
        """
        app = Application(self.config)

        # Rather than checking if the date is correct, which is a bit harder
        # when dealing with timezones, just check it against a regex instead.
        deb_date = app.get_package_date()
        self.assertRegexpMatches(
            deb_date, r'^[A-Za-z]+, \d+ [A-Za-z]+ \d+ \d+:\d+:\d+ [+-]\d+$')

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_get_architecture(self):
        """
        Based on the names of the .whl files in the wheelhouse directory,
        the get_architecture() method should return 'all' if all the wheels
        are pure-python only, or return 'any' if there are .whl files with
        binary code compiled for a specific architecture.
        """
        app = Application(self.config)
        app.wheelhouse_dir = TEST_DIR

        # touch some files, why does python not have os.touch()?
        open('Django-1.5.7-py2.py3-none-any.whl', 'w').close()
        open('django_filebrowser-3.5.6-py2-none-any.whl', 'w').close()

        # these are all pure python wheels
        self.assertEqual(app.get_architecture(), 'all')

        # add another wheel, this one architecture-specific
        open('psycopg2-2.5.2-cp27-none-linux_x86_64.whl', 'w').close()

        # any in Debian will build a .deb for each architecture
        self.assertEqual(app.get_architecture(), 'any')

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_format_description(self):
        """
        The description field in the wheelbarrow.json can either be a
        simple string, or a list.

        If it is a list, the format_description() method should return
        a string formatted for use in the Debian control file.
        """
        app = Application(self.config)

        desc1 = 'test description'
        desc2 = ['multiline', 'description']
        desc3 = ['another', 'multiline', 'description']

        # just a string, nothing should change
        description = app.format_description(desc1)
        self.assertEqual(description, desc1)

        # two lines, with an extra space on the second line
        description = app.format_description(desc2)
        self.assertEqual(description, 'multiline\n description')

        # more than two lines starts adding a dot between paragraphs
        description = app.format_description(desc3)
        self.assertEqual(description, 'another\n multiline\n .\n description')

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_create_debian_dir(self):
        """
        Test for the create_debian_dir() method.

        Mock out other functions and just check the logic in the
        create_debian_dir() method, to see if it creates the files
        we expect it to create.
        """
        expected_files = [
            'changelog',
            'compat',
            'control',
            'install',
            'postinst',
            'postrm',
            'rules',
        ]

        app = Application(self.config)
        app.debian_dir = os.path.join(TEST_DIR, 'debian')
        app.get_architecture = Mock(return_value='all')
        app.get_package_date = Mock(
            return_value='Mon, 12 May 2014 23:14:32 +1200')

        app.create_debian_dir()

        # just look at directory listing, that should be enough.
        self.assertListEqual(sorted(os.listdir('debian')), expected_files)

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_get_python_package(self):
        """
        Test the get_python_package() method, this method should return
        either the string "python" or "python3", which is the Debian package
        for the python version passed as the argument: python_version.
        """
        app = Application(self.config)
        self.assertEqual(app.get_python_package(2), 'python')
        self.assertEqual(app.get_python_package(3), 'python3')
        self.assertEqual(app.get_python_package(2.7), 'python')
        self.assertEqual(app.get_python_package(3.4), 'python3')

    @patch('wheelbarrow.app.Requirements', Mock())
    def test_get_python_binary(self):
        """
        Test the get_python_binary() method, this method should return
        either the path to the python binary for the given version.
        """
        app = Application(self.config)
        self.assertEqual(app.get_python_binary(2), '/usr/bin/python')
        self.assertEqual(app.get_python_binary(3), '/usr/bin/python3')
        self.assertEqual(app.get_python_binary(2.7), '/usr/bin/python2.7')
        self.assertEqual(app.get_python_binary(3.4), '/usr/bin/python3.4')

    @patch('subprocess.check_call')
    @patch('wheelbarrow.app.Requirements', Mock())
    def test_build_deb(self, check_call):
        """
        A method that calls lots of other methods in the same class
        and creates a .deb file by running dpkg-buildpackage.

        Create a lot of mocks during this test, so it doesn't turn out
        into a big smoke test that tests everything.
        """
        app = Application(self.config)
        app.basedir = os.path.join(TEST_DIR, 'wheelbarrow')
        app.build_dir = os.path.join(app.basedir, 'build')
        app.create_virtualenv = Mock()
        app.build_wheels = Mock()
        app.create_debian_dir = Mock()

        # create a fake .deb when calling subprocess.check_call(),
        # this allows us to test if the file gets copied when done.
        new_deb_name = 'myapp-wheels_0.2.deb'
        new_deb = os.path.join(app.basedir, new_deb_name)
        check_call.side_effect = lambda cmd, cwd: open(new_deb, 'w').close()

        # touch a .deb file in basedir (simulating old build artifacts),
        # the file should be removed when basedir is deleted and recreated.
        old_deb = os.path.join(app.basedir, 'myapp-wheels_0.1.deb')
        os.makedirs(app.basedir)
        open(old_deb, 'w').close()

        # run the method under test
        app.build_deb()

        # basedir should always be deleted and recreated,
        # we can tell because the old .deb file is deleted.
        self.assertFalse(os.path.exists(old_deb))
        self.assertTrue(os.path.exists(app.basedir))

        # build_dir is also created
        self.assertTrue(os.path.exists(app.build_dir))

        # all mocked functions are called once
        app.create_virtualenv.assert_called_once()
        app.build_wheels.assert_called_once()
        app.create_debian_dir.assert_called_once()

        # dpkg-buildpackage is also called and with the correct args
        check_call.assert_called_once_with(
            ['dpkg-buildpackage', '-us', '-uc'], cwd=app.build_dir)

        # check if .deb file was copied correctly after build
        # note that this is a copy, not move, so the file also exists
        # in the old location, this is intentional.
        copied_deb_file = os.path.join(TEST_DIR, new_deb_name)
        self.assertTrue(os.path.exists(new_deb))
        self.assertTrue(os.path.exists(copied_deb_file))
