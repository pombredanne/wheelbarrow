pip>=1.5
virtualenv>=1.11.2
wheel==0.23.0
pytz
python-magic==0.4.6
