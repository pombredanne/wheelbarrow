#!/usr/bin/env python
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()

with open(os.path.join(here, 'requirements/base.txt')) as f:
    requires = [l.strip() for l in f.readlines()]

with open(os.path.join(here, 'requirements/development.txt')) as f:
    dev_requires = [l.strip() for l in f.readlines()]

setup(
    name='wheelbarrow',
    version='0.1',
    description='Packages pip wheels from requirements.txt into a deb package',
    long_description=README,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Topic :: System :: Archiving :: Packaging',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    author='Rob van der Linde',
    author_email='robvdl@gmail.com',
    url='https://bitbucket.org/robvdl/wheelbarrow/',
    keywords='wheel deb packaging',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=dev_requires,
    extras_require={'dev': dev_requires},
    test_suite='nose.collector',
    scripts=['bin/wheelbarrow'],
)
