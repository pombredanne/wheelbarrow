import os
import json

# python 2/3 workaround
# in python 3, raw_input() is renamed to input()
# in python 2, input() has a different meaning and evals what the user types,
# this is very dangerous so we make input() work like python 3 in python 2.
try:
    input = raw_input
except NameError:
    pass


class Config(dict):
    """
    Stores the wheelbarrow.json configuration, as the Config class
    is based on a dictionary.

    Also provides methods to load json configuration, and generate a new
    wheelbarrow.json file.
    """
    filename = 'wheelbarrow.json'

    def __init__(self, **kwargs):
        """
        Config constructor loads the wheelbarrow.json file if it exists.
        """
        super(Config, self).__init__(**kwargs)

        if os.path.exists(self.filename):
            self.load_config()

    def input_text(self, *args, **kwargs):
        """
        Wrapper around the input() function.

        The main purpose for this wrapper method is so that it can be
        replaced by a Mock during testing, which doesn't work with builtins.
        """
        input(*args, **kwargs)

    def load_config(self):
        """
        Load the wheelbarrow.json configuration file.
        """
        # clear existing config
        self.clear()

        with open(self.filename, 'r') as f:
            self.update(json.load(f))

    def generate_new_config(self):
        """
        Ask the user a number of questions and generate a config file.
        """
        if os.path.exists(self.filename):
            result = self.input_text(
                'Config file {0} already exists, overwrite (y/N)? '
                .format(self.filename)).lower()

            if result != 'y':
                print('Aborted.')
                return

        # clear existing config
        self.clear()

        # now ask a number of questions
        self['package'] = {
            'name': self.input_text('Package name: '),
            'version': self.input_text('Package version: '),
            'description': self.input_text('Short package description: '),
            'author': self.input_text('Package author: '),
            'email': self.input_text('Author email: ')
        }
        self['requirements'] = ['requirements.txt']

        # write config file to disk
        self.save()
        print('Created config file: ' + self.filename)

    def save(self):
        """
        Save config file, also adds a newline at end of the file.
        """
        with open(self.filename, 'w') as f:
            json.dump(self, f, indent=4, sort_keys=True)
            f.write('\n')

    def get_python_version(self):
        """
        Return python version from the configuration, converted to a float.

        if the python version is not supported (<2, >=4), throw an exception.
        """
        python_version = float(self['package'].get('python', 2))
        if python_version < 2.0 or python_version >= 4.0:
            raise ValueError('Unsupported python version')
        return python_version
