import os
import glob
import shutil
import subprocess
import contextlib
from string import Template
from datetime import datetime

# I have considered using the requests library to avoid this
# annoying workaround, but that would just add another dependency,
# while urllib2 is at least built in, except under a different name.
try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

import magic
from pytz.reference import LocalTimezone

from wheelbarrow.requirements import Requirements
from wheelbarrow.utils import parse_dependency_link

# the magic library returns the mimetype, but we need the extension,
# so use a simple lookup table to map supported mimetypes to extensions.
MIMETYPE_EXTENSION_MAP = {
    'application/x-bzip2': '.tar.bz2',
    'application/gzip': '.tar.gz',
    'application/zip': '.zip'
}


class Application(object):

    basedir = '/tmp/wheelbarrow'
    virtualenv = os.path.join(basedir, 'venv')
    pip = os.path.join(virtualenv, 'bin/pip')
    python = os.path.join(virtualenv, 'bin/python')

    build_dir = os.path.join(basedir, 'build')
    debian_dir = os.path.join(build_dir, 'debian')
    template_dir = os.path.join(os.path.dirname(__file__), 'template')
    wheelhouse_dir = os.path.join(build_dir, 'wheelhouse')

    def __init__(self, config):
        self.config = config
        self.requirements = Requirements(config)

    def get_python_package(self, python_version):
        """
        Returns the debian package name for python, using the requested
        python version, returns either "python" or "python3".
        """
        if int(python_version) == 2:
            return 'python'
        else:
            return 'python3'

    def get_python_binary(self, python_version):
        """
        Returns the path to the python binary, using the requested version,
        returns /usr/bin/python, /usr/bin/python3, or /usr/bin/python3.4
        """
        if python_version == 2.0:
            return '/usr/bin/python'
        elif python_version == 3.0:
            return '/usr/bin/python3'
        else:
            return '/usr/bin/python%.1f' % python_version

    def build_deb(self):
        print('Starting wheelbarrow build...')

        # basedir contains the venv and build folders
        shutil.rmtree(self.basedir, True)
        os.mkdir(self.basedir)

        # create a new virtualenv
        self.create_virtualenv()

        # the build dir is where we build the debian package,
        # it is created inside the virtualenv folder.
        os.mkdir(self.build_dir)

        # build wheels into build_dir/wheelhouse,
        # this also creates a requirements.txt in this folder.
        self.build_wheels()

        # creating the debian dir involves copying over the template
        # directory and resolving variables in {{ }} braces.
        self.create_debian_dir()

        # finally, build .deb package
        subprocess.check_call(['dpkg-buildpackage', '-us', '-uc'],
                              cwd=self.build_dir)

        # and copy .deb into current dir (there should only be one .deb)
        for filename in glob.glob(os.path.join(self.basedir, '*.deb')):
            shutil.copy2(filename, '.')

        print('Done.')

    def create_virtualenv(self):
        """
        Create a virtualenv and install wheel.
        """
        # check_call will raise an exception if non-zero return code
        subprocess.check_call([
            'virtualenv',
            '-p',
            self.get_python_binary(self.config.get_python_version()),
            self.virtualenv
        ])

        # we need to install wheel in order to build wheels,
        # built not necessarily to install them.
        subprocess.check_call([self.pip, 'install', 'wheel'])

    def build_wheels(self):
        # full requirements.txt file, stored in the package, this includes
        # any dependencies coming from external sources (dependency_links)
        full_requirements = os.path.join(self.build_dir, 'requirements.txt')
        self.requirements.save(full_requirements, False)

        # requirements.txt file that only has the dependencies that come
        # from pypi (the ones that are installable using pip)
        pip_requirements = os.path.join(self.build_dir, 'pip_requirements.txt')
        self.requirements.save(pip_requirements, True)

        # build wheels from pypi first
        subprocess.check_call([
            self.pip,
            'wheel',
            '-r', pip_requirements,
            '-w', self.wheelhouse_dir
        ])

        # now build wheels managed by dependency_links
        self.build_manual_wheels()

    def build_manual_wheels(self):
        """
        Builds any wheels managed by dependency_links, these wheels
        are not in pypi and have to be downloaded and built manually.
        """
        # remember current dir so we can restore it later, as running
        # "setup.py bdist_wheel" requires us to change directories.
        old_dir = os.getcwd()

        dependency_links = self.config.get('dependency_links', [])
        for link in dependency_links:
            url, package, version = parse_dependency_link(link)
            pkg_and_version = '{0}-{1}'.format(package, version)
            unzip_dir = os.path.join(self.build_dir, pkg_and_version)
            os.mkdir(unzip_dir)

            # retrieve file
            print('Downloading {0} . . .'.format(url))
            with contextlib.closing(urlopen(url)) as infile:
                data = infile.read()

            # figure out what type of file this is, the url doesn't always
            # have an extension that tells us, so look at the file magic.
            mimetype = magic.from_buffer(data[:1024], mime=True)

            # get file extension from mimetype,
            # unsupported file types will just throw an exception.
            ext = MIMETYPE_EXTENSION_MAP[mimetype]
            filename = os.path.join(self.build_dir, pkg_and_version + ext)

            # save downloaded file
            with open(filename, 'wb') as outfile:
                outfile.write(data)

            # unzip and run "setup.py bdist_wheel"
            package_dir = self.unzip_manual_package(filename, unzip_dir)
            os.chdir(package_dir)
            subprocess.check_call([
                self.python,
                'setup.py',
                'bdist_wheel'
            ])

            # copy .whl file into wheelhouse dir with other wheels
            for whl in glob.glob(os.path.join(package_dir, 'dist', '*.whl')):
                shutil.copy(whl, self.wheelhouse_dir)

        # restore working dir
        os.chdir(old_dir)

    def unzip_manual_package(self, archive_file, unzip_dir):
        """
        Unzips a manually downloaded package, this could be either a .zip,
        .tar.gz, or a .tar.bz2 archive, so we need to pick the correct
        command to unzip the file.
        """
        # extensions come from MIMETYPE_EXTENSION_MAP so are always known
        if archive_file.endswith('.tar.bz2'):
            subprocess.check_call([
                'tar',
                'xjvf',
                archive_file,
                '-C',
                unzip_dir,
                '--strip-components=1'
            ])
            return unzip_dir

        elif archive_file.endswith('.tar.gz'):
            subprocess.check_call([
                'tar',
                'xzvf',
                archive_file,
                '-C',
                unzip_dir,
                '--strip-components=1'
            ])
            return unzip_dir

        elif archive_file.endswith('.zip'):
            # unfortunately unzip doesn't support --strip-components=1 to
            # flatten the base folder inside the archive, so the setup.py
            # could be in a subdirectory, which we then have to find...
            subprocess.check_call([
                'unzip',
                archive_file,
                '-d',
                unzip_dir
            ])

            # try to find setup.py in subdirectories first, most
            # packages have the setup.py in a subdirectory
            for filename in os.listdir(unzip_dir):
                subdir = os.path.join(unzip_dir, filename)
                setup_file = os.path.join(subdir, 'setup.py')
                if os.path.isdir(subdir) and os.path.exists(setup_file):
                    return subdir

            # otherwise, return base directory
            return unzip_dir

    def get_package_date(self):
        """
        Reads the system date and time, including the timezone and returns
        a formatted date string suitable for use in the Debian changelog file:

        Example output: Mon, 23 Dec 2013 11:41:00 +1200
        """
        today = datetime.now()
        localtime = LocalTimezone()
        date_with_timezone = today.replace(tzinfo=localtime)
        return date_with_timezone.strftime('%a, %d %b %Y %H:%M:%S %z')

    def get_architecture(self):
        """
        Checks the filename of each .whl file in the wheelhouse directory,
        the last part of the filename is the wheel architecture and is
        'any', only if it is a pure python wheel, otherwise it will be the
        architecture of the machine it was built on.

        If we only find pure python wheels in the directory, return 'all',
        this will build an architecture independent .deb package.

        If we find any wheel where the architecture is not 'any', we
        must build a debian package for each platform. This is done
        in the debian control file by using the architecture 'any',
        here it has a different meaning.
        """
        for filename in os.listdir(self.wheelhouse_dir):
            name, _ = os.path.splitext(filename)
            pkg_arch = name.split('-')[-1]

            # This bit is probably a bit confusing, 'any' in the wheel
            # filename means it is a pure-python wheel. If we find
            # wheels that are not pure-python, we return 'any' as well,
            # which in a debian control file means something different,
            # it means build a different package for each architecture.
            if pkg_arch != 'any':
                return 'any'

        # package contains only pure-python wheels.
        return 'all'

    def format_description(self, description):
        """
        If description is a list, convert to a multiline description
        in the correct format for debian control files.
        """
        if type(description) is list:
            # the description format is a bit odd, a dot is required
            # between lines, but not the first line.
            return '{0}\n {1}'.format(
                description[0],
                '\n .\n '.join(description[1:])
            )

        return description

    def create_debian_dir(self):
        os.mkdir(self.debian_dir)
        package = self.config['package']
        package_date = self.get_package_date()
        architecture = self.get_architecture()
        description = self.format_description(package['description'])
        python_version = self.config.get_python_version()

        # some scripts may not be implemented, fill in the blanks
        scripts = package.get('scripts', {})
        scripts.setdefault('pre_install', '')
        scripts.setdefault('post_install', '')
        scripts.setdefault('pre_upgrade', '')
        scripts.setdefault('pre_remove', '')
        scripts.setdefault('post_remove', '')

        # if any of the scripts are a list, convert them to one string
        for script in scripts.keys():
            if type(scripts[script]) is list:
                scripts[script] = '\n'.join(scripts[script])

        tmpl_vars = {
            'package_name': package['name'],
            'package_description': description,
            'package_version': package['version'],
            'package_author': package['author'],
            'package_email': package['email'],
            'package_architecture': architecture,
            'package_date': package_date,
            'python_bin': self.get_python_binary(python_version),
            'python_pkg': self.get_python_package(python_version),
            'pre_install': scripts['pre_install'],
            'post_install': scripts['post_install'],
            'pre_upgrade': scripts['pre_upgrade'],
            'pre_remove': scripts['pre_remove'],
            'post_remove': scripts['post_remove']
        }

        for filename in os.listdir(self.template_dir):
            src_filename = os.path.join(self.template_dir, filename)
            dest_filename = os.path.join(self.debian_dir, filename)

            with open(src_filename, 'r') as src:
                parsed = Template(src.read()).safe_substitute(**tmpl_vars)

            with open(dest_filename, 'w') as dest:
                dest.write(parsed)
