def parse_dependency_link(dependency_link):
    """
    Takes a string containing a dependency link in the same format
    found in setup.py files:

    http://someurl.com/tarball#egg=pkgname-0.1

    returns a tuple containing the url, package name, and version
    """
    url, package_and_version = dependency_link.split('#egg=')
    package, version = package_and_version.split('-')
    return url, package, version
