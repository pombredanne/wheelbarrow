import re

from wheelbarrow.utils import parse_dependency_link

RE_PACKAGE = re.compile(r'^([^!<>=,\[\]]*)(.*)$')


class Requirements(dict):

    def __init__(self, config, **kwargs):
        """
        Generates a new Requirements object.

        Unlike a normal dictionary, the Requirements class constructor
        takes an extra fist argument, which is a Config object.

        :param config: Config object, containing a wheelbarrow configuration
        """
        super(Requirements, self).__init__(**kwargs)
        self.dependency_links = config.get('dependency_links', [])
        self.parse_requirements(config['requirements'])

    def read_requirements_file(self, filename):
        """
        Reads a requirements.txt file and returns a list of lines.
        """
        with open(filename, 'r') as f:
            lines = f.readlines()

        # deal with the issue that readlines() returns newline chars
        return [l.replace('\n', '').replace('\r', '') for l in lines]

    def _iter_packages(self, lines):
        """
        Generator that takes lines from a requirements.txt file and loops
        over the lines, parsing them into tuples of (package, version).

        The generator ignores comments and blank lines, and only returns
        tuples of (package, version).
        """
        for line in lines:
            # start by stripping whitespace
            line = line.strip()

            # skip comments and blank lines
            if line.startswith('#') or line == '':
                continue

            # External repositories using -e are not supported yet.
            # However, you can list the dependency as normal (e.g. rvcms==0.4),
            # and link to a tarball using dependency_links in wheelbarrow.json
            if line.startswith('-'):
                raise ValueError(
                    'Unsupported line in requirements file: ' + line)

            # a standard package, with optional version or version range
            match = RE_PACKAGE.match(line)
            if not match:
                raise ValueError(
                    'Unsupported line in requirements file: ' + line)

            # Get package and version using regex, return with spaces removed.
            # We also lowercase the package name, to make it easier to find
            # duplicates later (as the package name is used as a dict key).
            package, version = match.groups()
            yield package.replace(' ', '').lower(), version.replace(' ', '')

    def parse_requirements(self, req_file_list):
        """
        Given a list of paths to requirements.txt files, read all the
        files into a single list of lines and then parse them into a
        dictionary with the package name as the key, and version as value.

        If a duplicate package is found, we check if the version is the same,
        if not, an exception is thrown.

        :param req_file_list: A list of paths to requirement.txt files.
        """
        # read all the requirements.txt files into a single list of lines
        all_lines = []
        for filename in req_file_list:
            all_lines.extend(self.read_requirements_file(filename))

        # empty existing dictionary first
        self.clear()

        for package, version in self._iter_packages(all_lines):
            # check for duplicates, except if the versions are the same
            if package in self and self[package] != version:
                raise ValueError(
                    'Duplicate dependency but different versions: ' + package)
            else:
                self[package] = version

    def save(self, filename, use_dependency_links=False):
        """
        Saves contents of Requirements dict back to a requirements.txt file.

        Generates a single file, even if multiple requirements.txt files
        were used as input.

        If use_dependency_links is True, don't save lines that are managed
        by dependency_links in the wheelbarrow.json file.

        :param use_dependency_links: remove lines managed by dependency_links
        """
        external_dependencies = \
            [parse_dependency_link(l)[1] for l in self.dependency_links]

        with open(filename, 'w') as f:
            for package, version in self.items():
                # this is an external dependency managed by dependency_links
                # don't add it if use_dependency_links is true
                if use_dependency_links and package in external_dependencies:
                    continue

                f.write('{0}{1}\n'.format(package, version))
